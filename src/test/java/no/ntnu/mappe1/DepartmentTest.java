package no.ntnu.mappe1;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Assertions;

public class DepartmentTest {
    private final Department department = new Department("Intensive Care Unit");
    private final Patient patient = new Patient("Spike","Spiegel","20440626");
    private final Employee employee = new Employee("Gon","Freecss","19990505");
    private final Employee surgeon = new Surgeon("Killua","Zoldyck","19990707");
    private final Nurse nurse = new Nurse("Yui","Hirasawa","19911127");

    @Nested
    class _add{

        @Test
        void can_add_employee() throws RemoveException {
            department.addEmployee(employee);
            Assertions.assertTrue(department.getEmployees().get(0).equals(employee));
        }

        @Test
        void can_add_patient() throws RemoveException {
            department.addPatient(patient);
            Assertions.assertTrue(department.getPatients().get(0).equals(patient));
        }

    }

    @Nested
    class _remove{

        @Test
        void can_remove_existing_employee() throws RemoveException {
            department.addEmployee(employee);
            department.remove(employee);
        }

        @Test
        void can_remove_existing_patient() throws RemoveException {
            department.addPatient(patient);
            department.remove(patient);
        }

        @Test
        void children_can_be_removed() throws RemoveException {
            department.addEmployee(nurse);
            department.remove(nurse);
        }

        @Test
        void remove_nonexistent_person() {
            department.addEmployee(surgeon);
            Assertions.assertThrows(RemoveException.class, () -> {
                department.remove(patient);
            });
        }
    }
}
