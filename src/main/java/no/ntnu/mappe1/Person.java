package no.ntnu.mappe1;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Creates a new instance of {@code Person}.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
    */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }


    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFullName() {
        return (getFirstName() + " " + getLastName());
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String toString() {
        return ("Name: " + getFullName() +
                "\nSocial Security Number: " + getSocialSecurityNumber());
    }
}
