package no.ntnu.mappe1;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();

    public Department(String name) {
        this.departmentName = name;
    }
    
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Add {@code Employee} to the list of employees.
     * @param employee
     * @return {@code True} if successfully added.
     *         {@code False} if not added.
     */
    public boolean addEmployee(Employee employee) {
        boolean result = false;

        if (!employees.contains(employee)) {
            employees.add(employee);
            result = true;
        }
        return result;
    }

    /**
     * Add {@code Patient} to the list of patients.
     * @param patient
     * @return {@code True} if successfully added.
     *         {@code False} if not added.
     */
    public boolean addPatient(Patient patient) {
        boolean result = false;

        if (!patients.contains(patient)) {
            patients.add(patient);
            result = true;
        }
        return result;
    }

    /**
     * Removes given {@code Person} from register
     * @param person
     * @throws RemoveException if person is not registered
     */
    public void remove(Person person) throws RemoveException {
        // Checks if person is employee and tries to remove given employee from list of employees
        if (person instanceof Employee) {
            boolean result = employees.remove((Employee) person);
            if (!result) {
                throw new RemoveException("Employee not registered");
            }

        } else if (person instanceof Patient) {
            // Checks if person is patient and tries to remove given patient from list of patients
            boolean result = patients.remove((Patient) person);
            if (!result) {
                throw new RemoveException("Patient not registered");
            }

        } else {
            throw new RemoveException("Person neither hospital employee or patient");
        }
    }

    public ArrayList<Employee> getEmployees() {
        return this.employees;
    }

    public ArrayList<Patient> getPatients() {
        return this.patients;
    }
    public String getDepartmentName() {
        return departmentName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}

        Department that = (Department) obj;
        return patients.equals(that.patients) && employees.equals(that.employees) && departmentName.equals(that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patients, employees, departmentName);
    }

    @Override
    public String toString() {
        String string = "Department: " + departmentName;

        string += "\nEmployees: ";
        for(Employee employee : employees) {
            string += "\n\t" + employee.getFullName() + " (" + employee.getSocialSecurityNumber() + ")";
        }

        string += "\nPatients: ";
        for(Patient patient : patients) {
            string += "\n\t" + patient.getFullName() + " (" + patient.getSocialSecurityNumber()+ ")";
        }
        return string;
    }
}
