package no.ntnu.mappe1;

/**
 * Inerface to be implemented by classes that are diagnosable by a {@code Doctor}.
 */
interface Diagnosable {
    public void setDiagnosis(String string);
}
