package no.ntnu.mappe1;

import java.util.ArrayList;

public class Hospital {
    private final String HOSPITAL_NAME;
    public ArrayList<Department> departments = new ArrayList<>();

    public Hospital(String name) {
        this.HOSPITAL_NAME = name;
    }

    public void addDepartments(Department department) {
        this.departments.add(department);
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public String getHOSPITAL_NAME() {
        return HOSPITAL_NAME;
    }

    @Override
    public String toString() {
        String string = "Hospital: " + HOSPITAL_NAME + "\nDepartments: ";

        for(Department department : departments){
            string += "\n  " + department.getDepartmentName();
        }
        return string;
    }
}
