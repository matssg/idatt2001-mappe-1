package no.ntnu.mappe1;

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return ("Nurse: " + 
                "\nName: " + getFullName() + 
                "\nSocial Security Number: " + getSocialSecurityNumber());
    }
}
