package no.ntnu.mappe1;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String message) {
        super("Could not remove person from register: " + message);
    }
}
