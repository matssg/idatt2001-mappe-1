package no.ntnu.mappe1;

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return ("Patient: " + 
                "\nName: " + getFullName() + 
                "\nSocial Security Number: " + getSocialSecurityNumber() +
                "\nDiagnosis: " + getDiagnosis());
    }
}
