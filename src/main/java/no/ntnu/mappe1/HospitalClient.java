package no.ntnu.mappe1;

public class HospitalClient {

    public static void fillRegisterWithTestData(final Hospital hospital) throws RemoveException {
        //Adds some departments with various employees and patients
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", ""));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", ""));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", ""));
        emergency.getPatients().add(new Patient("Inga", "Lykke", ""));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", ""));
        hospital.getDepartments().add(emergency);
        
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V." ,"Elvefølger", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", ""));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", ""));
        hospital.getDepartments().add(childrenPolyclinic);  
    }
    
    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("St. Olavs");
        //Fills register with test data
        fillRegisterWithTestData(hospital);

        //Prints out information about hospital/departments in register
        System.out.println(hospital.toString() + "\n");
        for (Department department : hospital.getDepartments()) {
            System.out.println(department.toString() + "\n");
        }

        //Removes employee 'Odd Even' from department 'Akutten'
        hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));

        //Prints out information again
        System.out.println(hospital.getDepartments().get(0).toString() + "\n");

        //Trties to remove nonexistent person from register
        try {
            hospital.getDepartments().get(0).remove(new Patient("Anya", "Taylor-Joy", "19960416"));
        } catch(Exception e) {
            System.out.println(e.toString());
        }
    }
}
