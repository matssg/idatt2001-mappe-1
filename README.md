# IDATx2001 Programmering 2 - Mappe 1

### Problemstilling

Inspirasjon til problemstillingen er hentet fra hjemmesiden til [St. Olavs Hospital](https://stolav.no/). Modellen vi skal jobbe med i denne oppgaven er forenklet for å tilpasses pensum og tidsrammen i denne innleveringen.

Definisjon hentet fra [Store Medisinske Leksikon](https://sml.snl.no/sykehus): _Sykehus er en helseinstitusjon som er godkjent for innleggelse, undersøkelse og behandling av pasienter og fødende kvinner som trenger spesialisert helsetjeneste. Til sykehusene hører sengeavdelinger, poliklinikker og laboratorier.Tradisjonelt har man skjelnet mellom somatiske sykehus, som hovedsakelig behandler kroppens sykdommer, og psykiatriske sykehus, som behandler psykiskelidelser. Dette skillet er av mindre betydning nå i og med at psykiatriske avdelinger ofte inngår som en del av det totale sykehustilbudet._

I denne oppgaven skal du kun jobbe med ett sykehus. Ett sykehusobjekt består av en liste med avdelinger og er identifisert med et navn. Hver avdeling består videre av en liste med ansatte og en liste med pasienter. Ansatte kan videre kategoriseres som helsepersonell og øvrige ansatte. Helsepersonell begrenses til allmennlege, kirurg og sykepleier. Det er kun allmennlege og kirurg som kan sette diagnose på en pasient. Du skal ikke lage en fullstendig klient i denne oppgaven, men en forenklet versjon som er spesifisert i oppgave 5.